package com.example.eleo.btree;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;


//assume inner Fragment for Activity
//make it implement OnClickListener to simply listen from several inputs
public class SearchFragment extends Fragment implements View.OnClickListener {
    private ArrayAdapter<String> mSearchAdapter;//make arrayAdapter to populate ListView
    StringBuilder text = new StringBuilder();//make StringBuilder for storing words from File
    String[] lines;//assume array of strings to read from file
    //get a reference to the listView and attach adapter to it
    ListView listView;//assume listView
    ArrayList<String> search_result = new ArrayList();//make arraylist for results
    TextView tv;//assume textView
    InputStream stream;//assume inputStream for reading from file

    public SearchFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_search, container, false);
        //get references to the buttons and attach click listeners to them
        Button generate = (Button) rootView.findViewById(R.id.button_generate);
        Button search = (Button) rootView.findViewById(R.id.button_search);
        generate.setOnClickListener(this);
        search.setOnClickListener(this);
        //create adapter for displaying the results in listView
        mSearchAdapter = new ArrayAdapter<String>(
                //fragment's parent activity
                getActivity(),
                //id of list item layout
                R.layout.list_item_search,
                //id of the textView to populate
                R.id.list_item_search_textview,
                //data to display(will be populated later)
                search_result
        );
        //make reference to the listView
        listView = (ListView) rootView.findViewById(
                R.id.listView_result);
        return rootView;
    }

    //handle events from buttons
    @Override
    public void onClick(View view) {
        //create Tree object (hovewer, doesn't work properly - app crashes null-pointer exception
        B_Tree tree = new B_Tree();
        //get references to buttons
        //switch the actions depending on buttons
        Button generate = (Button) view.findViewById(R.id.button_generate);
        Button search_button = (Button) view.findViewById(R.id.button_search);
        switch (view.getId()) {
            case R.id.button_generate:
                //check if the file exists on the sd card
                //read text File from resources to string array (split the stringBuilder by spaces)
                //(should be checked for the file not found exception)
                lines = readFromFile().toString().split(" ");
                for (String s : lines) {
                    //generate B-tree: insert word by word (assuming each word on each line)
                    tree.put(s);
                }
            case R.id.button_search:
                //if the result array is already not empty, clear it to display new items
                if (search_result != null)
                    mSearchAdapter.clear();
                tv = (TextView) view.findViewById(R.id.search);//make reference to search textView
                B_Tree.Word[] word;//make array of words (elements of each Node)
                //get the search string from input and call the search method of the tree
                word = tree.get(tv.getText().toString());
                search_result = tree.toArrayListSting(word);//convert word array to arrayList
                for(String str : search_result) {
                    mSearchAdapter.add(str);//populate adapter with search query results
                }
                listView.setAdapter(mSearchAdapter);//attach adapter to ListView reference
        }

    }

    //read word bank from prepared file in res/raw directory
    //@return StringBuilder of words separated by \n
    public StringBuilder readFromFile() {
        try {
            Context c = getActivity();//get current context to work within activity
            stream = c.getResources().openRawResource(R.raw.wordbank);//take resource from file
            //create reader for reading file
            BufferedReader br = new BufferedReader(new InputStreamReader(stream));
            String line;
            //read file line by line until eof, store in stringBuilder, separate each by \n
            while ((line = br.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
            stream.close();//close inputStream
            //catch FileNotFound and IOExceptions
        } catch
                (FileNotFoundException e) {
                e.printStackTrace();
            return null;
            //add Toast when the file not found
        } catch
                (IOException e) {
            e.printStackTrace();
        }
        return text;
    }

    //class representation for B-Tree
    public class B_Tree {
        public int M = 4; // max children per B-tree node = M-1
        public Node root; // root of the B-tree
        public int height; // height of the B-tree
        public int n; // number of elements in the B-tree

        //B-Tree node data type
        public class Node {
            public int c; // number of children
            public Word[] children = new Word[M]; //the array to store children
            public Node(int k) {
                this.c = k; // create a node with k children
            }
        }
        //internal nodes use word and reference to next node
        //external nodes use word and next is null
        public class Word {
            public String word;
            public Node next = null;

            public Word(String word, Node next) {
                this.word = word;
                this.next = next;
            }
            public String getValue() {
                return this.word;
            }

        }
        //transform Word array to ArrayList of Strings
        //@argument e is array to transform
        //@return ArrayList of String
        public ArrayList<String> toArrayListSting(B_Tree.Word[] e) {
            ArrayList<String> word = new ArrayList<String>();
            for (int i = 0; i < e.length - 1; i++)
                word.add(e[i].getValue());
            return word;
        }
        //initialize Tree: root element has 0 children
        public B_Tree() {
            this.root = new Node(0);
        }
        // return number of pairs in the B-tree
        public int size() {
            return this.n;
        }
        // return height of B-tree
        public int height() {
            return this.height;
        }

        public Node getRoot() {
            return this.root;
        }

        //search the input string
        //@argument a string to search
        //@return array of Word
        public Word[] get(String s) {
            return search(this.root, s, this.height);
        }

        // performs search only for the whole word
        public Word[] search(Node x, String word, int ht) {
            word.toLowerCase();//ignore case of the input word
            Word[] children = x.children;//make reference for the current node's children
            ArrayList<Word> ch = new ArrayList();
            //traverse external node (the height of the tree is 0)
            if (ht == 0) {
                for (int i = 0; i < x.c; i++) {
                    if (equal(word, children[i].word)) {
                        //if the input is equal to one of the children's name, return the whole node's children
                        //if (children[i].word.startsWith(word)){
                        // return children[i].word; //if we want to simply search a word
                        return children;
                    }
                }
            }
            //traverse internal node (the height is >0)
            else {
                for (int i = 0; i < x.c; i++) {
                    //if the word is smaller lexicografically, move to the second child
                    if (i + 1 == x.c || earlier(word, children[i + 1].word))
                        return search(children[i].next, word, ht - 1);
                }
            }
            return null;
        }

        //checks if the second word alphabetically bigger
        public boolean later(String a, String b) {
            return b.compareToIgnoreCase(a) > 0;
        }

        // insert word to the tree
        public void put(String word) {
            Node u = insert(this.root, word, this.height);
            this.n++;
            if (u == null)
                return;
            //if there's already max of children, need to split the root
            Node t = new Node(2);//create node with two children
            t.children[0] = new Word(this.root.children[0].word, this.root);//the first one is the first root's child next to root
            t.children[1] = new Word(u.children[0].word, u);//the second one is the current node's first child next to current node
            this.root = t;//make root pointed to created node
            this.height++;//increase the height of the tree
        }

        //insert word to the node
        public Node insert(Node h, String word, int ht) {
            int j;
            Word t = new Word(word, null);
            // check for duplicates in the tree: make sure that no such word exists
            if (get(word) != null)
                return null;
            // external node: don't insert if everyone are earlier
            if (ht == 0) {
                for (j = 0; j < h.c; j++) {
                    if (earlier(word, h.children[j].word))
                        break;
                }
            }
            //internal node: insert word to the node recursively
            else {
                for (j = 0; j < h.c; j++) {
                    if ((j + 1 == h.c) || earlier(word, h.children[j + 1].word)) {
                        Node u = insert(h.children[j++].next, word, ht - 1);
                        if (u == null)
                            return null;
                        t.word = u.children[0].word;
                        t.next = u;
                        break;
                    }
                }
            }

            //put current word to node's children
            for (int i = h.c; i > j; i--)
                h.children[i] = h.children[i - 1];
            h.children[j] = t;
            h.c++;
            //if the number of children is less than maximum, remain same
            if (h.c < this.M)
                return null;
            else
            //else, split the node
                return split(h);
        }

        // split node in half
        public Node split(Node h) {
            Node t = new Node(M / 2);
            h.c = M / 2;
            for (int j = 0; j < M / 2; j++)
                t.children[j] = h.children[M / 2 + j];
            return t;
        }

        //convert the whole tree to string, recursively
        public String toString() {
            return toString(this.root, this.height, "") + "\n";
        }

        public String toString(Node h, int ht, String indent) {
            String s = "";
            Word[] children = h.children;

            if (ht == 0) {
                for (int j = 0; j < h.c; j++) {
                    s += indent + children[j].word + " " + "\n";
                }
            } else {
                for (int j = 0; j < h.c; j++) {
                    if (j > 0)
                        s += indent + "(" + children[j].word + ")\n";
                    s += toString(children[j].next, ht - 1, indent + "     ");
                }
            }
            return s;
        }

        // check if one String is lexicographically less than another
        public boolean earlier(String s1, String s2) {
            return s2.compareToIgnoreCase(s1) < 0;
        }

        //check if one String is equal to another
        public boolean equal(String s1, String s2) {
            return s1.compareToIgnoreCase(s2) == 0;
        }

    }

}
